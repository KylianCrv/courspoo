<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //definition des constantes de connexion
        define('SERVEUR', 'localhost');
        define('UTILISATEUR', 'root');
        define('MOTDEPASSE', '');
        define('BD', 'bibliotheque');

        //connexion à la BD
        $cnx = new PDO('mysql:host=' . SERVEUR . ';dbname=' . BD, UTILISATEUR, MOTDEPASSE,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        //création d'une requete SQL

        $sql = 'SELECT nom, prenom,date_naissance FROM auteur';

//executer la requete sql

        $idRequete = $cnx->query($sql);

        while ($row = $idRequete->fetch(PDO::FETCH_ASSOC)) {
            echo $row['prenom'] . ' ' . $row['nom'] . ' ' . $row['date_naissance'] . ' <br> ';
        }
        $cnx = null;
        ?>
    </body>
</html>
