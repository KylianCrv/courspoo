<?php

/**
 * Description of hydratation
 *
 * 
 */
class hydratation {

    function hydrate(array $data) {

        foreach ($data as $k => $v) {
            //Concaténation : nom de la méthode Setter à appeler
            $method = 'set' . ucfirst($k);
            //Appel si et seulement si la méthode existe
            //Utiliser la fonction prédéfinie method_exists
            //Celle ci attend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $method)) {
                //invoquer la méthode
                $this->$method($v);
            }
        }
    }

    function __construct($data) {
        $this->hydrate($data);
    }

}
