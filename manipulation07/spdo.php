<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //definition des constantes de connexion
        define('SERVEUR', 'localhost');
        define('UTILISATEUR', 'root');
        define('MOTDEPASSE', '');
        define('BD', 'baseQuiMousse');

        //connexion à la BD
        $cnx = new PDO('mysql:host=' . SERVEUR . ';dbname=' . BD, UTILISATEUR, MOTDEPASSE,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        //création d'une requete SQL

        $sql = 'SELECT NomMarque, Version, NroType, CouleurBière, TauxAlcool, Caractéristiques FROM biere';

//executer la requete sql

        $idRequete = $cnx->query($sql);

        while ($row = $idRequete->fetch(PDO::FETCH_ASSOC)) {
            echo $row['NomMarque'] . ' ' . $row['Version'] . ' ' . $row['NroType'] . ' ' . $row['CouleurBière'] . ' ' . $row['TauxAlcool'] . ' ' . $row['Caractéristiques'] . ' <br> ';
        }
        $cnx = null;
        ?>
    </body>
</html>
