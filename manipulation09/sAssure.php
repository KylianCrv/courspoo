<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title></title>
    </head>
    <body>
        <?php
        //definition des constantes de connexion
        define('SERVEUR', 'localhost');
        define('UTILISATEUR', 'root');
        define('MOTDEPASSE', '');
        define('BD', 'assurance');

        require_once '../manipulation08/assureManager.php';
        require_once '../manipulation08/Assure.php';

        $cnx = new PDO('mysql:host=' . SERVEUR . ';dbname=' . BD, UTILISATEUR, MOTDEPASSE,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        $m = new assureManager($cnx);

        if (isset($_POST['btn_creer'])) {
            $assure = new Assure(['nom' => $_POST['f_nom'], 'age' => $_POST['f_age'], 'domicile' => $_POST['f_domicile'], 'bonusMalus' => 0, 'pointsFidelite' => 5,]);
            if ($assure->getNom() != null && $assure->getAge() != null && $assure->getDomicile() != null) {
                $m->addAssure($assure);
            }
        }
        if (isset($_POST['btn_regler'])) {
            $assure = $m->getAssure($_POST['f_id']);
            $assure->reglerAssurance();
            $m->editAssure($assure);
        }
        if (isset($_POST['btn_accident'])) {
            $assure = $m->getAssure($_POST['f_id']);
            $assure->avoirAccident();
            $m->editAssure($assure);
        }
        if (isset($_POST['btn_supprimer'])) {
            $assure = $m->getAssure($_POST['f_id']);

            $m->deleteAssure($assure);
        }
        ?>
        <strong>Les dossiers assurés</strong>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">N°</th>
                    <th scope="col">Nom</th>
                    <th scope="col">BonusMalus</th>
                    <th scope="col">PtsFidelité</th>
                    <th scope="col">Régler</th>
                    <th scope="col">Notification</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $assures = $m->getListAssure();

                if (empty($assures)) {
                    echo'Aucun assuré trouvé !';
                } else {
                    foreach ($assures as $unAssure) {
                        echo'<tr>
      <th scope="row">' . $unAssure->getIdAssure() . '</th>
      <td>' . $unAssure->getNom() . '</td>
      <td>' . $unAssure->getBonus() . '</td>
      <td>' . $unAssure->getPointsFidelite() . '</td>
<td><form action="sAssure.php" method=POST>
<input type="hidden" name="f_id" value="' . $unAssure->getIdAssure() . '">
    <button type="submit" class="btn btn-primary btn sm" name="btn_regler">Régler</button>
    </form>
    </td>
<td><form action="sAssure.php" method=POST>
<input type="hidden" name="f_id" value="' . $unAssure->getIdAssure() . '">
    <button type="submit" class="btn btn-primary btn sm" name="btn_accident">Accident</button>
    </form>
    </td>
<td><form action="sAssure.php" method=POST>
<input type="hidden" name="f_id" value="' . $unAssure->getIdAssure() . '">
    <button type="submit" class="btn btn-primary btn sm" name="btn_supprimer">Supprimer</button>
    </form>
    </td>
    </tr>';
                    }
                }
                ?> </tbody>
        </table>
        <form action="sAssure.php" method="POST">
            <label>Nom : </label><br>
            <input type="text" placeholder="Saisir le nom" value="" name="f_nom"><br>
            <label>Age : </label><br>
            <input type="number" placeholder="Saisir l'âge" value="" name="f_age"><br>
            <label>Domicile : </label><br>
            <input type="text" placeholder="Saisir le domicile" value="" name="f_domicile"><br>
            <input type="submit" value="Créer" name="btn_creer"class="btn btn-primary btn sm">
        </form>
    </body>
</html>
