<?php

/**
 * Description of Assure
 *
 *
 */
class Assure {

    private $nom = "Thierry";
    private $domicile = "Ceaulmont";
    private $age = 34;
    private $bonusMalus = 0;

    function reglerAssurance() {
        $this->setBonusMalus(4);
    }

    function parrainer(Assure $parraine) {
        if ($this->getBonus() > 0) {

            $parraine->setBonusMalus($this->getBonus());
        } else {

            $parraine->setBonusMalus(4);
        }
    }

    function avoirAccident() {
        $this->setBonusMalus(-14);
    }

    function getBonus() {
        return $this->bonusMalus;
    }

    function getNom() {
        return $this->nom;
    }

    function getDomicile() {
        return $this->domicile;
    }

    function getAge() {
        return $this->age;
    }

    function setNom($nom) {
        if (!is_string($nom) || empty($nom) || ctype_space($nom)) {
            trigger_error("Veuillez saisir un id correct", E_USER_WARNING);
            return;
        }
        $this->nom = $nom;
    }

    function setBonusMalus($bonusMalus) {
        if (!is_numeric($bonusMalus)) {
            trigger_error("Veuillez saisir un bonus malus correct", E_USER_WARNING);
            return;
        }
        if (($this->getBonus() + $bonusMalus) <= -50) {
            $this->bonusMalus = -50;
        } elseif (($this->getBonus() + $bonusMalus) >= 50) {
            $this->bonusMalus = 50;
        } else {
            $this->bonusMalus = $this->getBonus() + $bonusMalus;
        }
    }

}
