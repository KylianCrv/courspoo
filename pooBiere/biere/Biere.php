<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Biere
 *
 * @author kbidault
 */
class Biere {

    private $nomMarque;
    private $version;
    private $nroType;
    private $couleurBiere;
    private $tauxAlcool;
    private $caracteristiques;

    public function hyd($tabData) {
        foreach ($tabData as $k => $v) {
            //Concaténation : nom de la méthode Setter à appeler
            $method = 'set' . ucfirst($k);
            //Appel si et seulement si la méthode existe
            //Utiliser la fonction prédéfinie method_exists
            //Celle ci attend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $method)) {
                //invoquer la méthode
                $this->$method($v);
            }
        }
    }

    function __construct($tabData) {
        $this->hyd($tabData);
    }

// getter

    function getNomMarque() {
        return $this->nomMarque;
    }

    function getVersion() {
        return $this->version;
    }

    function getNroType() {
        return $this->nroType;
    }

    function getCouleurBiere() {
        return $this->couleurBiere;
    }

    function getTauxAlcool() {
        return $this->tauxAlcool;
    }

    function getCaracteristiques() {
        return $this->caracteristiques;
    }

//setter

    function setNomMarque($nomMarque) {
        $this->nomMarque = $nomMarque;
    }

    function setVersion($version) {
        $this->version = $version;
    }

    function setNroType($nroType) {
        $this->nroType = $nroType;
    }

    function setCouleurBiere($couleurBiere) {
        $this->couleurBiere = $couleurBiere;
    }

    function setTauxAlcool($tauxAlcool) {
        $this->tauxAlcool = $tauxAlcool;
    }

    function setCaracteristiques($caracteristiques) {
        $this->caracteristiques = $caracteristiques;
    }

}
