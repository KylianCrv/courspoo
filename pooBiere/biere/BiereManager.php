<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BiereManager
 *
 * @author kbidault
 */
class BiereManager {

    private $cnx;

    function __construct($cnx) {
        $this->setCnx($cnx);
    }

    function setCnx($cnx) {
        $this->cnx = $cnx;
    }

    function addBiere(Biere $biere) {
        $sql = 'INSERT INTO biere (nomMarque, Version, NroType, CouleurBiere, TauxAlcool, Caracteristiques) '
                . 'VALUES (?,?,?,?,?,?)';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($biere->getNomMarque(),
            $biere->getVersion(),
            $biere->getNroType(),
            $biere->getCouleurBiere(),
            $biere->getTauxAlcool(),
            $biere->getCaracteristiques()));
    }

    function deleteBiere(Biere $biere) {
        $sql = 'DELETE FROM biere WHERE Version = ?';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($biere->getVersion()));
    }

    function getListBiere() {
        //requete de type SELECT *
        $sql = 'SELECT * FROM biere, type  WHERE biere.nrotype = type.nrotype';
        $idRequete = $this->cnx->query($sql);
        while ($row = $idRequete->fetch(PDO::FETCH_ASSOC)) {
            $biere[] = new Biere($row);
        }
        return $biere;
    }

    function getBiere($id) {
        //requete de type SELECT 1 assuré
        $row = array(); //recuperation des données de l'utilisateur
        $id = (int) $id;

        $sql = 'SELECT * FROM biere WHERE Version = ? ';
        $idRequete = $this->cnx->prepare($sql); //requete préparée
        $idRequete->execute(array($id));
        $row = $idRequete->fetch(PDO::FETCH_ASSOC);

        return new Biere($row);
    }

}
