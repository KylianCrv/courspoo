<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

        <title></title>
    </head>
    <body>
        <?php
        //definition des constantes de connexion
        define('SERVEUR', 'localhost');
        define('UTILISATEUR', 'root');
        define('MOTDEPASSE', '');
        define('BD', 'basequimousse');

        require_once '../pooBiere/biere/BiereManager.php';
        require_once '../pooBiere/biere/Biere.php';
        $cnx = new PDO('mysql:host=' . SERVEUR . ';dbname=' . BD, UTILISATEUR, MOTDEPASSE,
                array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        $o = new BiereManager($cnx);

        if (isset($_POST['btn_supprimer'])) {
            $biere = $o->getBiere($_POST['f_id']);
            $o->deleteBiere($biere);
        }
        if (isset($_POST['btn_creer'])) {

            $biere = new Biere(['Version' => $_POST['f_version'],
                'nomMarque' => $_POST['f_nomMarque'],
                'nroType' => $_POST['f_type'],
                'couleurBiere' => $_POST['f_couleur'],
                'TauxAlcool' => $_POST['f_tauxAlcool'],
                'caracteristiques' => $_POST['f_carac'],]);
            if ($biere->getVersion() != null && $biere->getNomMarque() != null && $biere->getTauxAlcool() != null && $biere->getNroType() != null && $biere->getCouleurBiere() != null && $biere->getCaracteristiques() != null) {
                $o->addBiere($biere);
            }header('location:sBiere.php');
        }
        if (isset($_POST['btn_type'])) {
            
        }
        ?>
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Version</th>
                    <th scope="col">Nom Marque</th>
                    <th scope="col">Numéro Type</th>
                    <th scope="col">Couleur Bière</th>
                    <th scope="col">Taux Alcool</th>
                    <th scope="col">Caractéristiques</th>
                    <th scope="col">Supprimer</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $bieres = $o->getListBiere();
                if (empty($bieres)) {
                    echo'Aucunes bières trouvées';
                } else {
                    foreach ($bieres as $uneBiere) {
                        echo '<tr>
      <th scope="row">' . $uneBiere->getVersion() . '</th>
      <td>' . $uneBiere->getNomMarque() . '</td>
      <td>' . $uneBiere->getNroType() . '</td>
      <td>' . $uneBiere->getCouleurBiere() . '</td>
      <td>' . $uneBiere->getTauxAlcool() . '</td>
      <td>' . $uneBiere->getCaracteristiques() . '</td>
    <td>
    <form action="sBiere.php" method="POST">
<input type="hidden" name="f_id" value="' . $uneBiere->getVersion() . '">
    <button type="submit" class="btn btn-dark" name="btn_supprimer">Supprimer</button>
<input type="hidden" name="f_id" value="' . $uneBiere->getVersion() . '">
    <button type="submit" class="btn btn-dark" name="btn_type">Type</button>
    </form>
    </td>
    </tr>';
                    }
                }
                ?>
            </tbody>
        </table>
        <form action="sBiere.php" method="POST">
            <label>Version</label><br>
            <input type="text" name="f_version" value="" placeholder="Veuillez renseigner la version"><br>
            <label>Nom marque</label><br>
            <input type="text" name="f_nomMarque" value="" placeholder="Veuillez renseigner la marque"><br>
            <label>Type</label><br>
            <input type="text" name="f_type" value="" placeholder="Veuillez renseigner le type"><br>
            <label>Couleur</label><br>
            <input type="text" name="f_couleur" value="" placeholder="Veuillez renseigner la couleur"><br>
            <label>Taux Alcool</label><br>
            <input type="text" name="f_tauxAlcool" value="" placeholder="Veuillez renseigner le taux d'alcool"><br>
            <label>Caracteristiques</label><br>
            <input type="text" name="f_carac" value="" placeholder="Veuillez renseigner les caractéristiques"><br>
            <button type="submit" class="btn btn-success" name="btn_creer">Créer</button>
        </form>
    </body>
</html>
