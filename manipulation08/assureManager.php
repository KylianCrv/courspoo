<?php

/**
 * Description of assureManager
 * Gestion de la base de données
 * @author kbidault
 */
class assureManager {

    private $cnx;

    public function __construct($cnx) {
        $this->setCnx($cnx);
    }

    function setCnx($cnx) {
        $this->cnx = $cnx;
    }

    function addAssure(Assure $assure) {
        //Requete de type INSERT INTO
        $sql = 'INSERT INTO assure (nom, age, domicile, bonusMalus, pointsFidelite)'
                . 'VALUES (?,?,?,?,?)';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array(
            $assure->getNom(),
            $assure->getAge(),
            $assure->getDomicile(),
            $assure->getBonus(),
            $assure->getPointsFidelite()));
    }

    function editAssure(Assure $assure) {
        //type de requete UPDATE
        $sql = 'UPDATE assure SET nom = ?, age = ?,'
                . 'domicile = ?, bonusMalus = ?, '
                . 'pointsFidelite = ? WHERE idAssure = ?';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($assure->getNom(),
            $assure->getAge(),
            $assure->getDomicile(),
            $assure->getBonus(),
            $assure->getPointsFidelite(),
            $assure->getIdAssure()));
    }

    function deleteAssure(Assure $assure) {
        //requete de type DELETE
        $sql = 'DELETE FROM assure WHERE  idAssure = ? ';
        $idRequete = $this->cnx->prepare($sql);
        $idRequete->execute(array($assure->getIdAssure()));
    }

    function getListAssure() {
        //requete de type SELECT *
        $sql = 'SELECT * FROM assure';
        $idRequete = $this->cnx->query($sql);
        while ($row = $idRequete->fetch(PDO::FETCH_ASSOC)) {
            $assure[] = new Assure($row);
        }
        return $assure;
    }

    function getAssure($id) {
        //requete de type SELECT 1 assuré
        $row = array(); //recuperation des données de l'utilisateur
        $id = (int) $id;

        $sql = 'SELECT * FROM assure WHERE idAssure = ? ';
        $idRequete = $this->cnx->prepare($sql); //requete préparée
        $idRequete->execute(array($id));
        $row = $idRequete->fetch(PDO::FETCH_ASSOC);

        return new Assure($row);
    }

}
