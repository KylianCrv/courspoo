<?php

/**
 * Description of Assure
 *
 *
 */
class Assure {

    private static $information = "Tous les avantages ... <br>";
    private $nom;
    private $domicile;
    private $age;
    private $bonusMalus;
    private $pointsFidelite;
    private $idAssure;

    const BRONZE = 50;
    const ARGENT = 100;
    const OR = 150;

    public function hyd($tabData) {
        foreach ($tabData as $k => $v) {
            //Concaténation : nom de la méthode Setter à appeler
            $method = 'set' . ucfirst($k);
            //Appel si et seulement si la méthode existe
            //Utiliser la fonction prédéfinie method_exists
            //Celle ci attend 2 paramètres : l'objet en cours et le nom de la méthode
            if (method_exists($this, $method)) {
                //invoquer la méthode
                $this->$method($v);
            }
        }
    }

    function __construct($tabData) {
        $this->hyd($tabData);
    }

    function reglerAssurance() {
        $this->setBonusMalus(4);
        $this->setPointsFidelite(10);
    }

    function parrainer(Assure $parraine) {

        $this->setPointsFidelite(5);
        $parraine->setPointsFidelite(5);
    }

    function avoirAccident() {
        $this->setBonusMalus(-14);
    }

// getter
    static function getInformation() {
        return self::$information;
    }

    function getIdAssure() {
        return $this->idAssure;
    }

    function getBonus() {
        return $this->bonusMalus;
    }

    function getNom() {
        return $this->nom;
    }

    function getDomicile() {
        return $this->domicile;
    }

    function getAge() {
        return $this->age;
    }

    function getPointsFidelite() {
        return $this->pointsFidelite;
    }

// SETTER

    function setAge($age) {
//        if (!is_integer($age) || empty($age) || ctype_space($age)) {
//            trigger_error("Veuillez saisir un age correct", E_USER_WARNING);
//            return;
//        }
        $this->age = $age;
    }

    function setNom($nom) {
        if (!is_string($nom) || empty($nom) || ctype_space($nom)) {
            trigger_error("Veuillez saisir un id correct", E_USER_WARNING);
            return;
        }
        $this->nom = $nom;
    }

    function setDomicile($domicile) {
        if (!is_string($domicile) || empty($domicile) || ctype_space($domicile)) {
            trigger_error("Veuillez saisir un domicile correct", E_USER_WARNING);
            return;
        }
        $this->domicile = $domicile;
    }

    function setBonusMalus($bonusMalus) {
        if (!is_numeric($bonusMalus)) {
            trigger_error("Veuillez saisir un bonus malus correct", E_USER_WARNING);
            return;
        }
        if (($this->getBonus() + $bonusMalus) <= -50) {
            $this->bonusMalus = -50;
        } elseif (($this->getBonus() + $bonusMalus) >= 50) {
            $this->bonusMalus = 50;
        } else {
            $this->bonusMalus = $this->getBonus() + $bonusMalus;
        }
    }

    function setIdAssure($idAssure) {
        $this->idAssure = $idAssure;
    }

    function setPointsFidelite($pointsFidelite) {




        $this->pointsFidelite = $this->getPointsFidelite() + $pointsFidelite;
    }

}
